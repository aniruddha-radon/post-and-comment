import 'package:flutter/material.dart';
import 'package:http_project/screens/comment-list.dart';
import 'package:http_project/screens/post-list.dart';

Map<String, Widget Function(BuildContext)> routes = {
  PostListScreen.route: (context) => PostListScreen(),
  CommentListScreen.route: (context) => CommentListScreen()
};
