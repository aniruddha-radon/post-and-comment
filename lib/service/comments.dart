import 'dart:convert';

import 'package:http/http.dart' as http;

Future<List<Comment>> getComments(int id) async {
  String url =
      "https://jsonplaceholder.typicode.com/comments?postId=" + id.toString();
  Uri endpoint = Uri.parse(url);
  http.Response response = await http.get(endpoint);

  var usableData = jsonDecode(response.body);
  List<Comment> comments = createCommentData(usableData);
  return comments;
}

class Comment {
  final String name;
  final String comment;

  Comment(this.name, this.comment);
}

List<Comment> createCommentData(List<dynamic> data) {
  List<Comment> comments = [];
  for (var i = 0; i < data.length; i++) {
    var dataAtI = data[i];
    var name = dataAtI["name"];
    var body = dataAtI["body"];
    Comment comment = Comment(name, body);
    comments.add(comment);
  }
  return comments;
}
