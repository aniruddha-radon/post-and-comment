import 'dart:convert';

import 'package:http/http.dart' as http;

Future<List<Post>> getPosts() async {
  String url = "https://jsonplaceholder.typicode.com/posts";
  Uri endpoint = Uri.parse(url);
  http.Response response = await http.get(endpoint);

  var usableData = jsonDecode(response.body);
  List<Post> posts = createPostData(usableData);
  return posts;
}

class Post {
  final int id;
  final String title;
  final String description;

  Post(this.id, this.title, this.description);
}

List<Post> createPostData(List<dynamic> data) {
  List<Post> posts = [];
  for (var i = 0; i < data.length; i++) {
    var dataAtI = data[i];
    var id = dataAtI["id"];
    var title = dataAtI["title"];
    var body = dataAtI["body"];
    Post post = Post(id, title, body);
    posts.add(post);
  }
  return posts;
}
