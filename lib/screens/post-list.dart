import 'package:flutter/material.dart';
import 'package:http_project/screens/comment-list.dart';
import 'package:http_project/service/posts.dart';

class PostListScreen extends StatefulWidget {
  static final String route = "/postlist";
  const PostListScreen({Key? key}) : super(key: key);

  @override
  _PostListScreenState createState() => _PostListScreenState();
}

class _PostListScreenState extends State<PostListScreen> {
  List<Post> posts = [];

  void setPosts() async {
    List<Post> data = await getPosts();

    setState(() {
      posts = data;
    });
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    setPosts();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Posts"),
      ),
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: posts
              .map(
                (p) => GestureDetector(
                  onTap: () {
                    Navigator.pushNamed(context, CommentListScreen.route,
                        arguments: p.id);
                  },
                  child: Container(
                    decoration:
                        BoxDecoration(border: Border.all(color: Colors.black)),
                    padding: EdgeInsets.all(8.0),
                    margin: EdgeInsets.all(8.0),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          p.title,
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                          ),
                          textAlign: TextAlign.center,
                        ),
                        Text(p.description)
                      ],
                    ),
                  ),
                ),
              )
              .toList(),
        ),
      ),
    );
  }
}
