import 'package:flutter/material.dart';
import 'package:http_project/service/comments.dart';

class CommentListScreen extends StatefulWidget {
  static final String route = "/commentlist";
  const CommentListScreen({Key? key}) : super(key: key);

  @override
  _CommentListScreenState createState() => _CommentListScreenState();
}

class _CommentListScreenState extends State<CommentListScreen> {
  List<Comment> comments = [];
  bool isDataFetching = false;

  void setComments(int id) async {
    List<Comment> data = await getComments(id);

    setState(() {
      isDataFetching = true;
      comments = data;
    });
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    if (!isDataFetching) {
      var id = ModalRoute.of(context)!.settings.arguments as int;
      setComments(id);
    }

    return Scaffold(
      appBar: AppBar(
        title: Text("Comments"),
      ),
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: comments
              .map((c) => Container(
                    margin: EdgeInsets.all(8.0),
                    padding: EdgeInsets.all(8.0),
                    decoration:
                        BoxDecoration(border: Border.all(color: Colors.black)),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(c.comment),
                        Align(
                          alignment: Alignment.bottomRight,
                          child: Text(
                            c.name,
                            style: TextStyle(
                                color: Colors.green[300],
                                fontWeight: FontWeight.bold),
                          ),
                        )
                      ],
                    ),
                  ))
              .toList(),
        ),
      ),
    );
  }
}
