import 'package:flutter/material.dart';
import 'package:http_project/configurations/routes.dart';
import 'package:http_project/screens/post-list.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      initialRoute: PostListScreen.route,
      routes: routes,
    );
  }
}
